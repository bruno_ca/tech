using System;
using System.Collections.Generic;
using System.Linq;
using Tech.Domain.Entities;
using Tech.Domain.Interfaces;

namespace Tech.Domain.Services
{
    public class CartService : ICartService
    {
        private IList<Discount> _discounts;

        private IList<Article> _articles;
        private IList<DeliveryFees> _deliveryFees;

        public void Boostrap(IList<DeliveryFees> deliveryFees, IList<Article> articles, IList<Discount> discounts)
        {
            if(deliveryFees.Count == 0){
                throw new Exception("DeliveryFees e um valor obrigatorio");
            }
            if(articles.Count == 0){
                throw new Exception("Articles e um valor obrigatorio");
            }
            if(discounts.Count == 0){
                throw new Exception("DeliveryFees e um valor obrigatorio");
            }
            this._discounts = discounts;
            this._articles = articles;
            this._deliveryFees = deliveryFees;
        }

        

        public IList<Cart> CalculateDeliveryRates(IList<Cart> carts)
        {            
            foreach(var c in carts){
                var delivery = this.SelectedDelivery(c);
                c.Total += delivery.Price;
            }
            
            return carts;
        }

        private DeliveryFees SelectedDelivery(Cart cart){
            
           var delivery = this._deliveryFees
           .Where(d=>d.EligibleTransactionVolume.MaxPrice != null)
           .FirstOrDefault(d=> d.EligibleTransactionVolume.MinPrice <= cart.Total && d.EligibleTransactionVolume.MaxPrice > cart.Total);

           if(delivery == null){
                delivery =  this._deliveryFees.FirstOrDefault(x=>x.EligibleTransactionVolume.MaxPrice == null);
           }
           return delivery;
        }

        public IList<Cart> CalculateCart(IList<Cart> carts)
        {
            IList<Cart> cartsNew = new List<Cart>();
            foreach(var c in carts){
                var total = 0;                
                total = c.Items.Sum(x=> this.ApplyDiscount(this._articles.First(a=> a.Id == x.ArticleId).Price * x.Quantity,x.ArticleId));
                cartsNew.Add(new Cart(){
                    Id = c.Id,
                    Total = total
                });
            }
            return cartsNew;
        }
      
        public int CalculateDiscountType(int value, Discount discount){
            int d = 0;
            if(discount == null)
                return 0;
            
            switch(discount.Type){
                case "amount": 
                    d = 25;
                    break;
                case "percentage":
                    d =  value != 0 ? (int) (value*discount.Value)/100:  0;
                    break;
            }
            return d;            
        }
        public int ApplyDiscount(int value, int articleId){
            var discount = this._discounts.FirstOrDefault(x=>x.ArticleId == articleId);
            return value - this.CalculateDiscountType(value, discount);
        }   
    }
}