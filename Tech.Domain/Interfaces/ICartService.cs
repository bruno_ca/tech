using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tech.Domain.Entities;

namespace Tech.Domain.Interfaces
{
    public interface ICartService
    {
        IList<Cart> CalculateCart(IList<Cart> carts);
        IList<Cart> CalculateDeliveryRates(IList<Cart> carts);

        int ApplyDiscount(int value, int articleId);

        void Boostrap(IList<DeliveryFees> deliveryFees, IList<Article> articles, IList<Discount> discounts);
    }
}
