using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tech.Domain.Entities
{
    public class DeliveryFees
    {
        public int Price { get; set; }
        
        [JsonProperty("eligible_transaction_volume")]
        public EligibleTransactionVolume EligibleTransactionVolume { get; set; }
    }
}
