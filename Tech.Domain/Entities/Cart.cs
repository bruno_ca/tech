using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tech.Domain.Entities
{
    public class Cart
    {
        public int Id { get; set; }
        
        [JsonProperty("items", NullValueHandling=NullValueHandling.Ignore)]
        public IList<Item> Items { get; set; }

        public int Total { get; set; }
    }
}
