using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tech.Domain.Entities
{
    public class Item
    {
        [JsonProperty("article_id")]
        public int ArticleId { get; set; }
        public int Quantity { get; set; }
    }
}
