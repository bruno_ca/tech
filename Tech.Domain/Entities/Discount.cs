using System;
using Newtonsoft.Json;

namespace Tech.Domain.Entities
{
    public class Discount
    {   
        [JsonProperty("article_id")]
        public int ArticleId { get; set; }

        public String Type { get; set; }

        public int Value { get; set; }
    }
}
