using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tech.Domain.Entities
{
    public class EligibleTransactionVolume
    {
        [JsonProperty("max_price")]
        public int? MaxPrice { get; set; }
        
        [JsonProperty("min_price")]
        public int MinPrice { get; set; }
    }
}
