using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Tech.Domain.Entities;

namespace Tech.WebApi.Model
{
    public class ViewModelCart
    {
        public IList<Article> Articles { get; set; }
        public IList<Cart> Carts { get; set; }

        [JsonProperty("delivery_fees")]
        public IList<DeliveryFees> DeliveryFees { get; set; }

        public IList<Discount> Discounts { get; set; }
    }
}
