using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tech.Domain.Entities;
using Tech.WebApi.Model;

namespace Tech.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public JsonResult Post([FromBody]ViewModelCart  viewModelCart)
        {
            IList<Cart> ob = new List<Cart>();
            IList<Cart> carts = null;
            foreach(var c in viewModelCart.Carts){
                var total = 0;
                foreach(var i in c.Items){
                    var articles = viewModelCart.Articles.First(x=>x.Id == i.ArticleId);           
                    total =  articles.Price * i.Quantity;
                }
                ob.Add(new Cart(){
                    Id = c.Id,
                    Total = total
                });
                // carts =  CalculateDeliveryRates(ob, viewModelCart.DeliveryFees);
            }
            return Json(viewModelCart);
        }

        private IList<Cart> CalculateDeliveryRates(IList<Cart> carts, IList<DeliveryFees> deliveryFees)
        {
            foreach(var c in carts){
              var delivery = deliveryFees.First(x=>x.EligibleTransactionVolume.MaxPrice < c.Total && x.EligibleTransactionVolume.MinPrice > c.Total);
              c.Total += delivery.Price;
            }            
            return carts;
        }
                

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
