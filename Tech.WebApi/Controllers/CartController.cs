using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tech.Domain.Interfaces;
using Tech.Domain.Services;
using Tech.WebApi.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Tech.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CartController : Controller
    {
        ICartService cartService  = new CartService();

        // POST api/values
        [HttpPost]
        public JsonResult Post([FromBody]ViewModelCart cartModel)
        {
            cartService.Boostrap(cartModel.DeliveryFees, cartModel.Articles, cartModel.Discounts);
            var cartsCalcule = cartService.CalculateCart(cartModel.Carts);
            var cartWithDelivery = cartService.CalculateDeliveryRates(cartsCalcule);
            return Json(new {
                carts = cartsCalcule
            });
        }
    }
}
